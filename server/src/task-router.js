// @flow
import express, { response } from 'express';
import taskService from './task-service';
//import { request } from 'http';
const { spawn } = require('child_process');

/**
 * Express router containing task methods.
 */
const router: express$Router<> = express.Router();

router.post('/', (request, response) => {
  let stdout = '';
  let stderr = '';
  let exitcode: number = 0;

  const command = request.body.data;
  console.log(command);
  const docker = spawn('docker', ['run', '--rm', 'node-image', 'node', '-e', command]);

  docker.stdout.on('data', (data) => {
    console.log('stdout', data);
    stdout += data;
  });

  docker.stderr.on('data', (data) => {
    console.error('error', data);
    stderr += data;
  });

  docker.on('close', (code) => {
    exitcode += code;
    response.send({ stdout, stderr, exitcode });
  });
});

export default router;
