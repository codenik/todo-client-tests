// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route } from 'react-router-dom';
import { Alert, Card, Row, Column, Form, Button, NavBar } from './widgets';
import taskService from './task-service';

class App extends Component {
  data = '';
  output = { stdout: '', stderr: '', exitcode: null };
  render() {
    return (
      <>
        <Card title="App.js">
          <Row>
            <Column>
              <Form.Textarea
                type="text"
                value={this.data}
                onChange={(event) => (this.data = event.currentTarget.value)}
              />
            </Column>
          </Row>
          <Button.Success
            onClick={() => {
              taskService
                .create(this.data)
                .then((response) => {
                  this.output = response;
                  console.log(response);
                })
                .catch((error: Error) => Alert.danger('Error creating task: ' + error.message));
            }}
          >
            Run
          </Button.Success>
        </Card>

        <Card title="Standard output">
          <Row>
            <Column>{this.output.stdout}</Column>
          </Row>
        </Card>
        <Card title="Standard error">
          <Row>
            <Column>{this.output.stderr}</Column>
          </Row>
        </Card>
        <Card title="Exit status:">
          <Row>
            <Column>{this.output.exitcode}</Column>
          </Row>
        </Card>
      </>
    );
  }
}

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <HashRouter>
      <div>
        <Alert />
        <App />
      </div>
    </HashRouter>,
    root
  );
